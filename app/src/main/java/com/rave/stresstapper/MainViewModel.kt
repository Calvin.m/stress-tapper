package com.rave.stresstapper

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {

    //backing field
    //mutable means read and write
    private val _result = MutableStateFlow(0)
    val result: StateFlow<Int> get() = _result
    /*
    * var num1 = 5
    * var num2 = 10
    * val sum = num1 + num2
    * val sumB get() = num1 + num2
    *
    * num2 = 20
    * sum is still 15
    * sumB is updated to 25
    * when you use get, it reflects the expression everytime variables change
    * */

    fun increment() {
//        _result.value = _result.value++
        _result.value++
    }

    fun reset() {
        _result.value = 0
    }
}