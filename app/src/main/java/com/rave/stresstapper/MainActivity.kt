@file:OptIn(ExperimentalFoundationApi::class)

package com.rave.stresstapper

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rave.stresstapper.ui.theme.StressTapperTheme


class MainActivity : ComponentActivity() {
    //by signals a delegate
    private val mainViewModel by viewModels<MainViewModel>()

    //other by is used by lazy
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            StressTapperTheme {
                //collectAsState() is the observer
                val result: Int by mainViewModel.result.collectAsState()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .pointerInput(Unit) { detectTapGestures { } },
                    color = MaterialTheme.colorScheme.background
                ) {
                    ClickableSample(
                        result,
                        onClick = { mainViewModel.increment() },
                        onLongClick = { mainViewModel.reset() }
                    )
                }
            }
        }
    }
}

@Composable
//pass down LAMBDA
fun ClickableSample(
    result: Int,
    onClick: () -> Unit,
    onLongClick: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Row(
            Modifier
                .weight(1f)
                .padding(top = 24.dp)
        ) {
            Text(
                text = "Current count: ${result}",
                fontSize = 32.sp
            )
        }
        Row(Modifier.weight(2f)) {
            Button(
                onClick = onClick,
//                onLongClick = onLongClick,
                modifier = Modifier
                    .height(160.dp)
                    .width(200.dp)
                    .combinedClickable(
                        onClick = { },
                        onLongClick = onLongClick
                    )
            ) {
                Text(
                    text = "Click Me",
                    fontSize = 35.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .combinedClickable(
                            onClick = onClick,
                            onLongClick = onLongClick
                        )
                        .padding(top = 45.dp)
                )
            }
        }
        Row(Modifier.weight(1f)) {
            if (result > 0) {
                Text(
                    text = "Hold button reset counter",
                    fontSize = 24.sp
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    StressTapperTheme {
        ClickableSample(0,
            onClick = {}) {}
    }
}