package com.rave.stresstapper

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class MainViewModelTest {

    private val mainViewModel = MainViewModel()

    @Test
    @DisplayName("Test increment count when called")
    fun testIncrement() {
        //Given

        //When
        mainViewModel.increment()

        //Then
        Assertions.assertEquals(1, mainViewModel.result.value)
    }


    @Test
    @DisplayName("Test is reset")
    fun testRest() {
        //Given

        //When
        mainViewModel.increment()
        mainViewModel.increment()
        mainViewModel.reset()

        //Then
        Assertions.assertEquals(0, mainViewModel.result.value)
    }

}